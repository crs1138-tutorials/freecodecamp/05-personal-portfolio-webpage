# Changelog
All notable changes to the **05 Personal Portfolio Webpage** will be documented in this file. I'm using [semantic versioning](https://semver.org/) and the structure of this changelog is based on [keepachangelog.com](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
This section keeps track of upcoming changes and gives you a notice about what features you can expect in future.

## [1.0.0] - 2019-05-19

### Added
- adds the brief in the [README](./README.md) for the project
- adds this CHANGELOG file
- adds PackageJS localhost webserver for development environment
- creates basic file structure:
  * index.html - joins together the HTML, CSS and Javascript
  * style.css - provides styling for all the elements of the webpage
  * grid.css - adds layout grid
  * functions.js - creates a file for all the Javascript functions