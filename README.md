# 05 Build a Personal Portfolio Webpage

This is the last of the five projects needed to get my HTML+CSS certification from FreeCodeCamp. This project is personal portfolio and I am actually recycling the [01 Tribute Page](http://planxdesign.eu/freecodecamp/01-tribute-page). I've spent so much time planning and designing the first project without actually reading through the requirements that it hit me quite hard when I found out that great part of the work was unnecessary. The page however fits the bill for this project almost perfectly.

* Objective: Build a [CodePen.io](https://codepen.io) app that is functionally similar to this: https://codepen.io/freeCodeCamp/full/zNBOYG.

* Fulfill the below [user stories](https://en.wikipedia.org/wiki/User_story) and get all of the tests to pass. Give it your own personal style.

* You can use HTML, JavaScript, and CSS to complete this project. Plain CSS is recommended because that is what the lessons have covered so far and you should get some practice with plain CSS. You can use Bootstrap or SASS if you choose. Additional technologies (just for example jQuery, React, Angular, or Vue) are not recommended for this project, and using them is at your own risk. Other projects will give you a chance to work with different technology stacks like React. We will accept and try to fix all issue reports that use the suggested technology stack for this project. Happy coding!

* _User Story #1_: My portfolio should have a welcome section with an `id` of `welcome-section`.

* _User Story #2_: The welcome section should have an `h1` element that contains text.

* _User Story #3_: My portfolio should have a projects section with an `id` of `projects`.

* _User Story #4_: The projects section should contain at least one element with a `class` of `project-tile` to hold a project.

* _User Story #5_: The projects section should contain at least one link to a project.

* _User Story #6_: My portfolio should have a navbar with an `id` of `navbar`.

* _User Story #7_: The navbar should contain at least one link that I can click on to navigate to different sections of the page.

* _User Story #8_: My portfolio should have a link with an `id` of `profile-link`, which opens my GitHub or FCC profile in a new tab.

* _User Story #9_: My portfolio should have at least one media query.

* _User Story #10_: The height of the welcome section should be equal to the height of the viewport.

* _User Story #11_: The navbar should always be at the top of the viewport.

* You can build your project by forking [this CodePen pen](http://codepen.io/freeCodeCamp/pen/MJjpwO). Or you can use this CDN link to run the tests in any environment you like: https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js

* Once you're done, submit the URL to your working project with all its tests passing.

* Remember to use the [Read-Search-Ask](https://forum.freecodecamp.org/t/how-to-get-help-when-you-are-stuck/19514) method if you get stuck.