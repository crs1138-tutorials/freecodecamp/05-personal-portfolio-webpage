(function(){
  const buttons = document.querySelectorAll('#scrollDownButton, .nav-link');

  buttons.forEach(button => {
    button.addEventListener('click', function(eve) {
      eve.preventDefault();
      const target = (!!eve.target.hash) ? eve.target.hash : `#aboutEmma`;
      const targetElement = document.querySelector(target);
      scrollToElement(targetElement);
    });
  });

  function scrollToElement(element) {
    element.scrollIntoView({ behavior: 'smooth'});
  }
}());
